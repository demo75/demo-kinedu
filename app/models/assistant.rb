class Assistant < ApplicationRecord
  has_many :activity_logs, dependent: :destroy
end